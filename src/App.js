import React from "react";
import './App.css';
import CardDeck from './Poker/CardDeck.js';
import Card from "./Card";

class App extends React.Component {
    state = {
        cards: [],
    };


    createCardDeck = () => {
        const deck = new CardDeck();
        const cards52 = deck.cards;
        deck.getCards(5);
        const hand = deck.hand;

        this.setState({
            cards: hand
        });
    }

    render() {
        const res = this.state.cards.map(card => {   // card = {suit: '', rank: ''}
            return (
                <Card
                    rank={card.rank}
                    suit={card.suit}
                />
            )
        })

        return (
            <div className="App">
                {res}
                <button onClick={this.createCardDeck} type="button" className="btn">distribution of cards</button>
            </div>
        );
    }
}

export default App;
